from __future__ import division
import numpy as np
import cv2

from random import randint
import matplotlib.pyplot as plt
import imutils

class Util: 
    def __init__(self):
        pass
    
    # import the necessary packages

    def order_points(self, pts):
        # initialzie a list of coordinates that will be ordered
        # such that the first entry in the list is the top-left,
        # the second entry is the top-right, the third is the
        # bottom-right, and the fourth is the bottom-left
        rect = np.zeros((4, 2), dtype = "float32")
    
        # the top-left point will have the smallest sum, whereas
        # the bottom-right point will have the largest sum
        s = pts.sum(axis = 1)
        rect[0] = pts[np.argmin(s)]
        rect[2] = pts[np.argmax(s)]
    
        # now, compute the difference between the points, the
        # top-right point will have the smallest difference,
        # whereas the bottom-left will have the largest difference
        diff = np.diff(pts, axis = 1)
        rect[1] = pts[np.argmin(diff)]
        rect[3] = pts[np.argmax(diff)]
    
        # return the ordered coordinates
        return rect

    def four_point_transform(self, image, pts):
        # obtain a consistent order of the points and unpack them
        # individually
        rect = self.order_points(pts)
        (tl, tr, br, bl) = rect
    
        # compute the width of the new image, which will be the
        # maximum distance between bottom-right and bottom-left
        # x-coordiates or the top-right and top-left x-coordinates
        widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        maxWidth = max(int(widthA), int(widthB))
    
        # compute the height of the new image, which will be the
        # maximum distance between the top-right and bottom-right
        # y-coordinates or the top-left and bottom-left y-coordinates
        heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxHeight = max(int(heightA), int(heightB))
    
        # now that we have the dimensions of the new image, construct
        # the set of destination points to obtain a "birds eye view",
        # (i.e. top-down view) of the image, again specifying points
        # in the top-left, top-right, bottom-right, and bottom-left
        # order
        dst = np.array([
            [0, 0],
            [maxWidth - 1, 0],
            [maxWidth - 1, maxHeight - 1],
            [0, maxHeight - 1]], dtype = "float32")
    
        # compute the perspective transform matrix and then apply it
        M = cv2.getPerspectiveTransform(rect, dst)
        warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
    
        # return the warped image
        return warped
    def applyMask(self, frame):
        
        gray =  cv2.GaussianBlur(frame,(5,5),0)
        #ret3,bin = cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
        binary = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 51, -10)
     
        return binary
    def getCorners(self, frame):
        binary = self.applyMask(frame.copy())
      
        im2, contours, hierarchy = cv2.findContours(binary, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        for i in contours:
            rect = cv2.minAreaRect(i)
            box = cv2.boxPoints(rect)
            box = np.int0(box)
            x, y, w, h = cv2.boundingRect(i)
            card = frame[y: y + h, x: x + w]
            wraped = self.four_point_transform(frame, box)
           
            
            
            cv2.drawContours(frame, [box], 0, (0, 0, 255), 2)
        return frame;
        
        
        pass
class Card: 
    
    def __init__(self, cardTrainDataFile = "card.data", cardResponseDataFile = "cardResponses.data"):
        self.CARD_TRAIN_DATA_FILE = cardTrainDataFile
        self.CARD_RESPONSE_DATA_FILE = cardResponseDataFile
        self.initKnn()
    def initKnn(self):
        
        samples = np.loadtxt(self.CARD_TRAIN_DATA_FILE,np.float32)
        responses = np.loadtxt(self.CARD_RESPONSE_DATA_FILE,np.float32)
        responses = responses.reshape((responses.size,1))

        self.model = cv2.ml.KNearest_create()
        self.model.train(samples, cv2.ml.ROW_SAMPLE, responses)
        
    def train(self, img):
        image = img.copy()
        binary = self.applyMask(img)
        im2, countours, hirarchy = cv2.findContours(binary.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        countours = sorted(countours, key=cv2.contourArea, reverse=True)
        samples = np.empty((0, 1200))
        response = [] 
        countoursNum = len(countours); 
        counter = 0; 
        util = Util()
        while counter < countoursNum: 
            rect = cv2.minAreaRect(countours[counter])
            box = cv2.boxPoints(rect)
            box = np.int0(box)
            wraped_image = util.four_point_transform(image, box)
            
            x, y, w, h = cv2.boundingRect(countours[counter])
#             croped_image = image[y: y + h, x:x+w];
                
            resizedImage = cv2.resize(wraped_image.copy(), (300, 100));
           
            img2 = img.copy()
           
            cv2.rectangle(img2, (x, y), (x + w, y + h), (0, 0, 255), 3)
            img2 = imutils.resize(img2, width=500)
           
    #         digitsMarked = edgeDetection.mark_edge(resizedImage, edge)
    #         cv2.imshow("Digits marked" ,digitsMarked)
            cv2.imshow("Image", img2)
            cv2.imshow("Resized image ", resizedImage); 
           
            key  = cv2.waitKey(0)
            print key
            if key == 113: 
                break;
            if key == 115: 
                fileName = ("data/" + str(randint(0, 1000000))) + ".jpg";
                
                cv2.imwrite(fileName, resizedImage)
              
            elif key == 2424832: 
                if counter != 0: 
                    counter -= 1;
                continue
            
            elif key == 49: 
                
                response.append(1)
                print "is card"
        
            elif key == 48: 
                response.append(0)
                print "not card"
           
            if key == 49 or key == 48: 
                 
                sample_image = cv2.resize(wraped_image.copy(), (60, 20));
                
                sample = sample_image.reshape((1, 1200)); 
                samples = np.append(samples, sample, 0);
            
            
            counter += 1;
            
    
            
           
            
      
       
        response = np.array(response,np.float32)
        responses = response.reshape((response.size,1))
        with open(self.CARD_TRAIN_DATA_FILE, "a") as f_handle:
                np.savetxt(f_handle,samples)
        with open(self.CARD_RESPONSE_DATA_FILE, "a") as f_handle: 
            np.savetxt(f_handle,responses)
            
    def showData(self, newEntry = None):
        samples = np.loadtxt(self.CARD_TRAIN_DATA_FILE,np.float32)
        responses = np.loadtxt(self.CARD_RESPONSE_DATA_FILE,np.float32)
        responses = responses.reshape((responses.size,1))
        
        red = samples[responses.ravel() == 0]
        plt.scatter(red[:,0], red[:,1], 80, 'r', '^')
        
        blue = samples[responses.ravel() == 1]
        plt.scatter(blue[:,0], blue[:,1], 80, 'b', 's')
        
        plt.legend([red, blue])
        if newEntry != None:
            plt.scatter(newEntry[:, 0], newEntry[:, 1], 80, 'g', 'o')
        
        plt.show()
    def applyMask(self, frame):
        
        gray =  cv2.GaussianBlur(frame,(5,5),0)
        #ret3,bin = cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
        binary = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 51, -10)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5,5))
        binary = cv2.morphologyEx(binary,cv2.MORPH_OPEN, kernel)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (11,11))
        binary = cv2.morphologyEx(binary,cv2.MORPH_CLOSE, kernel)
        return binary
    def isCard(self, sample):
        sample_image = cv2.resize(sample.copy(), (60, 20));
        sample = sample_image.reshape((1, 1200)); 
        roismall = np.float32(sample)
        results = self.model.findNearest(roismall, k = 5)[1]
        return int(results[0][0])
    
    def extractCard(self, frame, orgFrame):
        binary = self.applyMask(frame.copy())
       
        countours = cv2.findContours(binary.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[1]
        selectedContours = []
        util = Util()
        
        for i in countours: 
            rect = cv2.minAreaRect(i)
            box = cv2.boxPoints(rect)
            box = np.int0(box)
            wraped_image = util.four_point_transform(frame, box)
            x, y, w, h = cv2.boundingRect(i)
#             croped_image = frame[y: y + h, x:x+w];
            
            if self.isCard(wraped_image): 
                
                
                
                cv2.rectangle(orgFrame, (x, y), (x + w, y + h), (0, 255, 255), 3)
                selectedContours.append(i)
        return orgFrame, selectedContours
    
    
    
    
    
class Field: 
    def __init__(self, fieldTrainDataFile = "cardField.data", fieldResponseDataFile = "cardFieldResponses.data"):
        self.FIELD_TRAIN_DATA_FILE = fieldTrainDataFile
        self.FIELD_RESPONSE_DATA_FILE = fieldResponseDataFile
        self.initKnn()
        
    def initKnn(self):
        samples = np.loadtxt(self.FIELD_TRAIN_DATA_FILE,np.float32)
        responses = np.loadtxt(self.FIELD_RESPONSE_DATA_FILE,np.float32)
        responses = responses.reshape((responses.size,1))
        self.model = cv2.ml.KNearest_create()
        self.model.train(samples, cv2.ml.ROW_SAMPLE, responses)
        
    def applyMask(self, frame):

        gray =  cv2.GaussianBlur(frame,(5,5),0)
        #ret3,bin = cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
        binary = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 51, -10)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5,5))
        binary = cv2.morphologyEx(binary,cv2.MORPH_OPEN, kernel)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (11,11))
        binary = cv2.morphologyEx(binary,cv2.MORPH_CLOSE, kernel)
        return binary
    def showData(self, newEntry = None):
        samples = np.loadtxt(self.FIELD_TRAIN_DATA_FILE,np.float32)
        responses = np.loadtxt(self.FIELD_RESPONSE_DATA_FILE,np.float32)
        responses = responses.reshape((responses.size,1))
        
        red = samples[responses.ravel() == 0]
        plt.scatter(red[:,0], red[:,1], 80, 'r', '^')
        
        blue = samples[responses.ravel() == 1]
        plt.scatter(blue[:,0], blue[:,1], 80, 'b', 's')
        if newEntry != None:
            plt.scatter(newEntry[:, 0], newEntry[:, 1], 80, 'g', 'o')
        
        plt.show()
    def isField(self, sample):
        sample_image = cv2.resize(sample.copy(), (60, 20));
        sample = sample_image.reshape((1, 1200)); 
        roismall = np.float32(sample)
        results = self.model.findNearest(roismall, k = 5)[1]
        return int(results[0][0])
    def train(self, img):
        image = img.copy()
        binary = self.applyMask(img)
        im2, countours, hirarchy = cv2.findContours(binary.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        countours = sorted(countours, key=cv2.contourArea, reverse=True)
        samples = np.empty((0, 400))
        response = [] 
        countoursNum = len(countours); 
        counter = 0; 
        while counter < countoursNum: 
            x, y, w, h = cv2.boundingRect(countours[counter])
            croped_image = image[y: y + h, x:x+w];
                
            resizedImage = cv2.resize(croped_image.copy(), (300, 100));
           
            img2 = img.copy()
           
            cv2.rectangle(img2, (x, y), (x + w, y + h), (0, 0, 255), 3)
            img2 = imutils.resize(img2, width=500)
           
    #         digitsMarked = edgeDetection.mark_edge(resizedImage, edge)
    #         cv2.imshow("Digits marked" ,digitsMarked)
            cv2.imshow("Image", img2)
            cv2.imshow("Resized image ", resizedImage); 
           
            key  = cv2.waitKey(0)
            print key
            if key == 113: 
                break;
            if key == 115: 
                fileName = ("data/" + str(randint(0, 1000000))) + ".jpg";
                
                cv2.imwrite(fileName, resizedImage)
              
            elif key == 2424832: 
                if counter != 0: 
                    counter -= 1;
                continue
            
            elif key == 49: 
                
                response.append(1)
                print "is card"
        
            elif key == 48: 
                response.append(0)
                print "not card"
           
            if key == 49 or key == 48: 
                 
                sample_image = cv2.resize(croped_image.copy(), (60, 20));
                
                sample = sample_image.reshape((1, 1200)); 
                samples = np.append(samples, sample, 0);
            
            
            counter += 1;
        
        response = np.array(response,np.float32)
        responses = response.reshape((response.size,1))
        with open(self.FIELD_TRAIN_DATA_FILE, "a") as f_handle:
                np.savetxt(f_handle,samples)
        with open(self.FIELD_RESPONSE_DATA_FILE, "a") as f_handle: 
            np.savetxt(f_handle,responses)
    def extractField(self, frame, orgFrame, cardCordinate):
        
        
        util = Util()
        binary = self.applyMask(frame)
        countours = cv2.findContours(binary.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[1]
        selectedContours = []
        fieldsDetectd = []
        
        for i in countours: 
            rect = cv2.minAreaRect(i)
            box = cv2.boxPoints(rect)
            box = np.int0(box)
            wraped_image = util.four_point_transform(frame, box)    
           
            x, y, w, h = cv2.boundingRect(i)
            croped_image = frame[y: y + h, x:x+w];
            if self.isField(wraped_image):  
                
                
                fieldsDetectd.append(wraped_image)
                cv2.rectangle(orgFrame, (x + cardCordinate[0], y + cardCordinate[1]), (x + w + cardCordinate[0], y + h + cardCordinate[1]), (255, 0, 0), 1)
                selectedContours.append(i)
        return orgFrame, selectedContours, fieldsDetectd
        
        
    
class Number: 
    TOLERANCE = 2
    RANGE = 2.5
    def __init__(self, numberTrainDataFile = "digit.data", numberResponseDataFile = "digitResponse.data"):
        self.NUMBER_TRAIN_DATA_FILE = numberTrainDataFile
        self.NUMBER_RESPONSE_DATA_FILE = numberResponseDataFile
        self.initKnn()
        
    def initKnn(self):
        samples = np.loadtxt(self.NUMBER_TRAIN_DATA_FILE,np.float32)
        responses = np.loadtxt(self.NUMBER_RESPONSE_DATA_FILE,np.float32)
        responses = responses.reshape((responses.size,1))
        self.model = cv2.ml.KNearest_create()
        self.model.train(samples, cv2.ml.ROW_SAMPLE, responses)
    def showData(self, newEntry = None):
        samples = np.loadtxt(self.NUMBER_TRAIN_DATA_FILE,np.float32)
        responses = np.loadtxt(self.NUMBER_RESPONSE_DATA_FILE,np.float32)
        responses = responses.reshape((responses.size,1))
        
        
        red = samples[responses.ravel() == 0]
        plt.scatter(red[:,0], red[:,1], 80, 'r', '^')
        
        blue = samples[responses.ravel() == 1]
        plt.scatter(blue[:,0], blue[:,1], 80, 'b', 's')
        if newEntry != None:
            plt.scatter(newEntry[:, 0], newEntry[:, 1], 80, 'g', 'o')
        
        plt.show()
    def predictDigit(self, sample):
        sample_image = cv2.resize(sample, (20, 20));
           
        sample = sample_image.reshape((1, 400)); 
        roismall = np.float32(sample)
        results = self.model.findNearest(roismall, k = 5)[1]
        return results[0][0]
    def sortContours(self, cnts, method="left-to-right"):
        reverse = False
        i = 0
    
        # handle if we need to sort in reverse
        if method == "right-to-left" or method == "bottom-to-top":
            reverse = True
    
        # handle if we are sorting against the y-coordinate rather than
        # the x-coordinate of the bounding box
        if method == "top-to-bottom" or method == "bottom-to-top":
            i = 1
    
        # construct the list of bounding boxes and sort them from top to
        # bottom
        boundingBoxes = [cv2.boundingRect(c) for c in cnts]
        (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
            key=lambda b:b[1][i], reverse=reverse))
    
        # return the list of sorted contours and bounding boxes
        return (cnts, boundingBoxes)
    
    def applyMask(self, frame):
        
       
#         gray =  cv2.GaussianBlur(frame,(15, 15),0)
        #ret3,bin = cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
        binary = cv2.adaptiveThreshold(frame, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 1)
        
        
#         kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5,5))
#         binary = cv2.morphologyEx(binary,cv2.MORPH_OPEN, kernel)
#         kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (11,11))
#         binary = cv2.morphologyEx(binary,cv2.MORPH_CLOSE, kernel)
        return binary
    def extractNumbers(self, frame):
    
        binary = self.applyMask(frame)
      
        countours = cv2.findContours(binary.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[1]
        countours = sorted(countours, key=cv2.contourArea, reverse=True)
        countours= self.sortContours(countours)[0]
        sampleHeight = frame.shape[0]
        number = ""
        for i in countours: 
           
            x, y, w, h = cv2.boundingRect(i) 
            number = frame[y: y + h, x: x + w]
            ratio = sampleHeight / h
            if ratio > self.RANGE - self.TOLERANCE and ratio < self.RANGE + self.TOLERANCE: 
#                 cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 255), 1)
                print self.predictDigit(number)
                
                
        
        return frame
    def train(self, img):
        image = img.copy()
        binary = self.applyMask(img)
        im2, countours, hirarchy = cv2.findContours(binary.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        countours = sorted(countours, key=cv2.contourArea, reverse=True)
        samples = np.empty((0, 400))
        response = [] 
        countoursNum = len(countours); 
        counter = 0; 
        while counter < countoursNum: 
            x, y, w, h = cv2.boundingRect(countours[counter])
            croped_image = image[y: y + h, x:x+w];
                
            resizedImage = cv2.resize(croped_image.copy(), (100, 100));
           
            img2 = img.copy()
           
            cv2.rectangle(img2, (x, y), (x + w, y + h), (0, 0, 255), 3)
            img2 = imutils.resize(img2, width=500)
           
    #         digitsMarked = edgeDetection.mark_edge(resizedImage, edge)
    #         cv2.imshow("Digits marked" ,digitsMarked)
            cv2.imshow("Image", img2)
            cv2.imshow("Resized image ", resizedImage); 
           
            key  = cv2.waitKey(0)
            print key
            if key == 113: 
                break;
            if key == 115: 
                fileName = ("data/" + str(randint(0, 1000000))) + ".jpg";
                
                cv2.imwrite(fileName, resizedImage)
              
            elif key == 2424832: 
                if counter != 0: 
                    counter -= 1;
                continue
            elif key == 45:
                print "not digit"
                response.append(-1)
            elif key >= 48 and key <=57:
                print "digit: " + str(key - 48);  
                response.append(key - 48);
            if key >= 48 and key <=57 or key == 45: 
                 
                sample_image = cv2.resize(croped_image.copy(), (20, 20));
                
                sample = sample_image.reshape((1, 400)); 
                samples = np.append(samples, sample, 0);
            
            
            counter += 1;
            
    
            
           
            
      
       
        response = np.array(response,np.float32)
        responses = response.reshape((response.size,1))
        with open(self.NUMBER_TRAIN_DATA_FILE, "a") as f_handle:
                np.savetxt(f_handle,samples)
        with open(self.NUMBER_RESPONSE_DATA_FILE, "a") as f_handle: 
            np.savetxt(f_handle,responses)
            pass    
