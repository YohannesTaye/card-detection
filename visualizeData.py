'''
Created on Aug 11, 2016

@author: Yohannes Taye
'''
import cv2
import numpy as np
import matplotlib.pyplot as plt

def showData(newEntry):
    samples = np.loadtxt('card.data',np.float32)
    responses = np.loadtxt('cardResponses.data',np.float32)
    responses = responses.reshape((responses.size,1))
    
    red = samples[responses.ravel() == 0]
    plt.scatter(red[:,0], red[:,1], 80, 'r', '^')
    
    blue = samples[responses.ravel() == 1]
    plt.scatter(blue[:,0], blue[:,1], 80, 'b', 's')
    
    plt.scatter(newEntry[:, 0], newEntry[:, 1], 80, 'g', 'o')
    
    plt.show()


